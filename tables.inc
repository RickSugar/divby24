

divBy24.branch.tbl          .incbin "branch_detect.tbl"
divBy24.msb_div.tbl.lo      .incbin "msb_lo_shift.tbl"
divBy24.msb_div.tbl.hi      .incbin "msb_hi_shift.tbl"
divBy24.lsb_Div.tbl.base    .incbin "lsb_shift.tbl"
divBy24.lsb_Div.tbl.error1  .incbin "error1_correction.tbl"
divBy24.lsb_Div.tbl.error2  .incbin "error2_correction.tbl"

;// MOD stuffs
divBy24.mod.tbl.noerror    .incbin "mod_noerror.tbl"
divBy24.mod.tbl.error1     .incbin "mod_error1.tbl"
divBy24.mod.tbl.error2     .incbin "mod_error2.tbl"
