
  .bank 0
  .org $e000

divisor = $40     ;$59 used for hi-byte
dividend = $44	  ;$fc used for hi-byte
remainder = $48  ;$fe used for hi-byte
result = dividend ;save memory by reusing divident to store the result

first_result = $00

test1 = $04
test2 = $05
test3 = $06

msb = $20
lsb = $21

startup:
    ldx #$ff
    txs
    lda #$00
    tam #$00
    lda #$f8
    tam #$01

    stz $00
    tii $2000,$2001, $2000


    jsr test_full_div
    jsr test_just_div
    jsr test_just_mod

.loop
    bra .loop


;//......................................................................
test_full_div:
        ldx #$00
        ldy #$00

        stz <divisor
        stz <divisor+1
        stz <dividend
        stz <dividend+1
        stz <remainder
        stz <remainder+1
        stz <test1
        stz <test2
        stz <test3

.loop
          phx
          phy

        stx <msb
        sty <lsb

        sty <dividend
        stx <dividend+1
        lda #24
        sta <divisor
        stz <divisor+1

      jsr DivBy24
        sty <first_result
        sta <first_result+1
        stx <first_result+2

      jsr longDivision

        lda <remainder
        cmp <first_result+2
      beq .skip1
        lda <test1
        ora #$01
        sta <test1
.skip1

        lda <first_result
        cmp <result
        lda <first_result+1
        sbc <result+1
      beq .skip2
        lda <test1
        ora #$80
        sta <test1
.skip2

          ply
          plx
        iny
      bne .loop
        inx
      bne .loop
  rts



;//......................................................................
test_just_div:
        ldx #$00
        ldy #$00

        stz <divisor
        stz <divisor+1
        stz <dividend
        stz <dividend+1
        stz <test1
        stz <test2
        stz <test3

.loop
          phx
          phy

        stx <msb
        sty <lsb

        sty <dividend
        stx <dividend+1
        lda #24
        sta <divisor
        stz <divisor+1

      jsr DivBy24.noMod
        sty <first_result
        sta <first_result+1
        stz <first_result+2

      jsr longDivision

        lda <first_result
        cmp <result
        lda <first_result+1
        sbc <result+1
      beq .skip2
        lda <test2
        ora #$80
        sta <test2
.skip2

          ply
          plx
        iny
      bne .loop
        inx
      bne .loop
  rts


;//......................................................................
test_just_mod:
        ldx #$00
        ldy #$00

        stz <divisor
        stz <divisor+1
        stz <dividend
        stz <dividend+1
        stz <remainder
        stz <remainder+1
        stz <test1
        stz <test2
        stz <test3

.loop
          phx
          phy

        stx <msb
        sty <lsb

        sty <dividend
        stx <dividend+1
        lda #24
        sta <divisor
        stz <divisor+1

      jsr ModBy24
        stx <first_result+2

      jsr longDivision

        lda <remainder
        cmp <first_result+2
      beq .skip1
        lda <test3
        ora #$01
        sta <test3
.skip1


          ply
          plx
        iny
      bne .loop
        inx
      bne .loop
  rts


;//......................................................................
; Source: codebase64.org

longDivision:

.divide	lda #0	        ;preset remainder to 0
    sta <remainder
    sta <remainder+1
    ldx #16	        ;repeat for each bit: ...

.divloop	
    asl <dividend	;dividend lb & hb*2, msb -> Carry
    rol <dividend+1	
    rol <remainder	;remainder lb & hb * 2 + msb from carry
    rol <remainder+1
    lda <remainder
    sec
    sbc <divisor	;substract divisor to see if it fits in
    tay	        ;lb result -> Y, for we may need it later
    lda <remainder+1
    sbc <divisor+1
	bcc .skip	;if carry=0 then divisor didn't fit in yet

    sta <remainder+1	;else save substraction result as new remainder,
    sty <remainder	
    inc <result	;and INCrement result cause divisor fit in 1 times

.skip	
    dex
  	bne .divloop	
	rts




.include "tables.inc"
.include "lib.asm"


;/////////////////////////////////////////////////////////////////////////////////
;/////////////////////////////////////////////////////////////////////////////////
;/////////////////////////////////////////////////////////////////////////////////
;
;// Interrupt routines

;//........
TIRQ:   ;// Not used
        stz $1403
        rti

;//........
BRK:
        rti

;//........
VDC:
          pha
        lda $0000
          pla
  rti

;//........
NMI:
        rti

;end INT


;/////////////////////////////////////////////////////////////////////////////////
;/////////////////////////////////////////////////////////////////////////////////
;/////////////////////////////////////////////////////////////////////////////////
;
;// INT VECTORS

  .org $fff6

  .dw BRK
  .dw VDC
  .dw TIRQ
  .dw NMI
  .dw startup

;..............................................................................................................
;..............................................................................................................
;..............................................................................................................
;..............................................................................................................
;Bank 0 end
