# DivBy24

A simple divide by constant 24 over a 16bit input range.

It's based on x/24 = (x & 0xff00)/24 + (x & 0xff)/24. But floor((x & 0xff00)/24) + floor((x & 0xff)/24) != x/24... it's off by 1 in some cases. The off by 1 is a based on a specific pattern, which a branching table uses to "error correct" by looking at the top byte of X as in X >> 8. 

MOD is done in a similar way, but combines a few logical steps into a single table. For instance, if you only take the mod of 24 against the LSB of a 16bit value, you get three repeating patterns. The first entry in the pattern is correct, the second is off by 8 and needs a (result +8) % 24, and the third needs (result +16) % 24. I've included the corrections in the tables. Again, the MSB of the 16bit range is simply used to dictate which table the LSB will load from (of the three).

The off by 1 in the div method follows the exact same pattern as the MOD method, and thus can use the same branching table.

The pattern for both MOD and DIV looks like this:

* $000 - $0ff => no errors
* $100 - $1ff => has a specific error pattern (error 1)
* $200 - $2ff => has a specific error pattern (error 2)
* $300 - $3ff => no error pattern
* $400 - $4ff => error 1 pattern
* $500 - $5ff => error 2 pattern
* $600 - $6ff => no error pattern
* etc..

# Pattern

At first glance, the pattern appears to be based on the fact that 3 is the largest "smallset" factor of 24. But the pattern appears to be a little more complicated than that; it appears to be based on taking the set {smallest_factors} in C, and multipling all smallest factors that are prime numbers. So if a number had the factor set {2,3,5} then the pattern size would be 15.

# Code

 The example code is for the HuC6280 PCEAS assembler (a derivative of the Rockwell 65C02S), but the tables should work for any processor.